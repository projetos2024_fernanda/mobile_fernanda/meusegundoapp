import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Button,
} from "react-native";

function SegundaTela({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Segunda Tela</Text>
      <Button title="Voltar para Home" onPress={() => navigation.goBack()} />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: "column", //coluna ou linha, define a direção
    justifyContent: "center",
    alignItems: "center",
  },
});

export default SegundaTela
