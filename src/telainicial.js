import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  Button,
} from "react-native";

function TelaInicial({ navigation }) {
  return (
    <View style={styles.container}>
      <Text>Tela Inicial</Text>
      <Button
        title="Ir para segunda página"
        onPress={() => navigation.navigate("SegundaTela")}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: "column", //coluna ou linha, define a direção
    justifyContent: "center",
    alignItems: "center",
  },
});

export default TelaInicial
