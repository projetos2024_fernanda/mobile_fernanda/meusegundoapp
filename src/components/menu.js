import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView, TouchableOpacity } from "react-native";
import { Button } from "react-native-web";

export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('TelaInicial')}>
        <Text>Opção 1</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.menu} onPress={()=> navigation.navigate('SegundaTela')}>
        <Text>Opção 2</Text>
      </TouchableOpacity>
      <TouchableOpacity style={styles.menu}>
        <Text>Opção 3</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // flexDirection: "column", //coluna ou linha, define a direção
    justifyContent: "center",
    alignItems: "center",
  },

  menu:{ 
    padding: 10,
    margin:5,
    backgroundColor: "pink",
    borderRadius: 5,
  },
});
