import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";

export default function Layout() {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text>MENU</Text>
      </View>

      <View style={styles.content}>
        <ScrollView>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
          <Text>CONTEÚDO</Text>
        </ScrollView>
      </View>

      <View style={styles.footer}>
        <Text>RODAPÉ</Text>
      </View>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column", //coluna ou linha, define a direção
    justifyContent: "space-between", //
  },

  header: {
    height: 50, //altura
    backgroundColor: "purple",
  },

  content: {
    flex: 1,
    backgroundColor: "pink",
  },

  footer: {
    height: 50,
    backgroundColor: "purple",
  },

});
